package com.example.BankSpringBoot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Nitu {
    private Float interest;
   @Autowired
   PersonBank personBank;
    public Nitu() {
    }
    public void calculatInterest(){
        if(personBank.getAge() >=70){
            this.interest= Float.valueOf((8.0f/100)*personBank.getBalance());
        }else
            this.interest= Float.valueOf((4.0f/100)*personBank.getBalance());

    }
    public void showInterest(){
        System.out.println("Interest amount: "+this.interest);
    }
    public Nitu(Float intrest) {
        this.interest = intrest;
    }

    @Override
    public String toString() {
        return "Nitu{" +
                "intrest=" + interest +
                '}';
    }

    public Float getIntrest() {
        return interest;
    }

    public void setIntrest(Float intrest) {
        this.interest = intrest;
    }
}
